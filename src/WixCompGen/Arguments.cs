﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Arguments.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using Proligence.Helpers.Console;

    /// <summary>
    /// Represents the application's arguments.
    /// </summary>
    public class Arguments
    {
        /// <summary>
        /// Gets the name of the property generated to reference the Wix fragment.
        /// </summary>
        [ApplicationArgument("FragmentPropertyName", false)]
        public string FragmentPropertyName { get; private set; }

        /// <summary>
        /// Gets the identifier of the container directory.
        /// </summary>
        [ApplicationArgument("ContainerDirectoryId", false)]
        public string ContainerDirectoryId { get; private set; }

        /// <summary>
        /// Gets the root directory for source files.
        /// </summary>
        [ApplicationArgument("SourceRoot", false)]
        public string SourceRoot { get; private set; }

        /// <summary>
        /// Gets the identifier of the component group which will contain the generated components.
        /// </summary>
        [ApplicationArgument("ComponentGroupId", false)]
        public string ComponentGroupId { get; private set; }

        /// <summary>
        /// Gets the full path to the root source directory.
        /// </summary>
        [ApplicationArgument("SourcePath", true)]
        public string SourcePath { get; private set; }

        /// <summary>
        /// Gets the name of the output file.
        /// </summary>
        [ApplicationArgument("Output", true)]
        public string Output { get; private set; }
        
        /// <summary>
        /// Gets the prefix for WiX identifiers.
        /// </summary>
        [ApplicationArgument("IdPrefix", false)]
        public string IdPrefix { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the application is in WiX update mode.
        /// </summary>
        [ApplicationSwitch("Update")]
        public bool UpdateMode { get; private set; }
    }
}
﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PathEventArgs.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using System;

    /// <summary>
    /// Implements a <see cref="EventArgs"/> structure for path and directory related events.
    /// </summary>
    public sealed class PathEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathEventArgs"/> class.
        /// </summary>
        /// <param name="path">The path.</param>
        public PathEventArgs(string path)
        {
            this.Path = path;
        }

        /// <summary>
        /// Gets the path associated with the event.
        /// </summary>
        public string Path { get; private set; }
    }
}
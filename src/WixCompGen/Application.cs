﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Application.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Proligence.Helpers.Console;

    /// <summary>
    /// Implements the console application
    /// </summary>
    public class Application : ConsoleApplication<Arguments>
    {
        /// <summary>
        /// Implements the application's logic.
        /// </summary>
        protected override void Execute()
        {
            try
            {
                var parameters = new WixGenerationParameters
                {
                    ComponentGroupId = this.Arguments.ComponentGroupId,
                    ContainerDirectoryId = this.Arguments.ContainerDirectoryId,
                    FragmentPropertyName = this.Arguments.FragmentPropertyName,
                    IdPrefix = this.Arguments.IdPrefix,
                    SourceRoot = this.Arguments.SourceRoot
                };

                string outputFileName = Path.GetFileNameWithoutExtension(Path.GetFileName(this.Arguments.Output));

                if (string.IsNullOrEmpty(parameters.FragmentPropertyName))
                {
                    parameters.FragmentPropertyName = outputFileName + "Components";
                }

                if (string.IsNullOrEmpty(parameters.ContainerDirectoryId))
                {
                    parameters.ContainerDirectoryId = "dir_" + outputFileName;
                }

                if (string.IsNullOrEmpty(parameters.SourceRoot))
                {
                    parameters.SourceRoot = "$(var." + outputFileName + ".TargetDir)";
                }

                if (string.IsNullOrEmpty(parameters.ComponentGroupId))
                {
                    parameters.ComponentGroupId = "cg_" + outputFileName + "Components";
                }

                XmlDocument existingWix = null;
                if (this.Arguments.UpdateMode)
                {
                    existingWix = LoadWixDocument(this.Arguments.Output);
                }

                using (var stream = File.Create(this.Arguments.Output))
                {
                    var settings = new XmlWriterSettings { Indent = true, Encoding = Encoding.UTF8 };
                    var writer = XmlWriter.Create(stream, settings);

                    var generator = new FileSystemWixGenerator(this.Arguments.SourcePath, writer, parameters)
                    {
                        Wix = existingWix
                    };

                    generator.DirectoryVisited += (sender, e) => this.Logger.Debug("DIR  " + e.Path);
                    generator.FileVisited += (sender, e) => this.Logger.Debug("FILE " + e.Path);
                    generator.GenerateWix();

                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Loads a WiX document.
        /// </summary>
        /// <param name="path">The path to the WiX document file.</param>
        /// <returns>The parsed XML document.</returns>
        private static XmlDocument LoadWixDocument(string path)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(path);

            return xmlDocument;
        }
    }
}
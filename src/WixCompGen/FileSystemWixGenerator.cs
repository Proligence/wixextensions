﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileSystemWixGenerator.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using NLog;
    using Proligence.Tools.Wix.Common;

    /// <summary>
    /// Walks through directories and files and generates Wix components for them.
    /// </summary>
    public class FileSystemWixGenerator : WixGenerator
    {
        /// <summary>
        /// The object used to log progress and diagnostic messages.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The path to the root directory.
        /// </summary>
        private readonly string rootPath;

        /// <summary>
        /// Wix generation parameters.
        /// </summary>
        private readonly WixGenerationParameters parameters;

        /// <summary>
        /// Maps component ids to their GUIDs.
        /// </summary>
        private IDictionary<string, Guid> componentGuids;

        /// <summary>
        /// Maps component GUIDs to their ids.
        /// </summary>
        private IDictionary<Guid, string> componentIds;

        /// <summary>
        /// Maps directory IDs to their full paths.
        /// </summary>
        private IDictionary<string, string> directoryPaths;

        /// <summary>
        /// Maps directory paths to their IDs.
        /// </summary>
        private IDictionary<string, string> directoryPathToIds;

        /// <summary>
        /// Maps file IDs to their full paths.
        /// </summary>
        private IDictionary<string, string> filePaths;

        /// <summary>
        /// Maps file paths to their IDs.
        /// </summary>
        private IDictionary<string, string> filePathToIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemWixGenerator"/> class.
        /// </summary>
        /// <param name="path">The path to the root directory.</param>
        /// <param name="writer">The XML writer to which generated Wix will be written.</param>
        /// <param name="parameters">Wix generation parameters.</param>
        public FileSystemWixGenerator(string path, XmlWriter writer, WixGenerationParameters parameters)
            : base(writer)
        {
            this.rootPath = path;
            this.parameters = parameters;
        }

        /// <summary>
        /// An event which is triggered for each visited directory.
        /// </summary>
        public event EventHandler<PathEventArgs> DirectoryVisited;

        /// <summary>
        /// An event which is triggered for each visited file.
        /// </summary>
        public event EventHandler<PathEventArgs> FileVisited;

        /// <summary>
        /// Gets or sets the existing WiX source file which will be updated.
        /// </summary>
        public XmlDocument Wix { get; set; }

        /// <summary>
        /// Generates the Wix XML.
        /// </summary>
        public override void GenerateWix()
        {
            if (this.Wix != null)
            {
                Logger.Info("Parsing existing WiX document");
                this.ParseExistingWix();
            }

            Logger.Info("Generating new WiX document");
            this.GenerateNewWix();
        }

        /// <summary>
        /// Raises the <see cref="DirectoryVisited" /> event.
        /// </summary>
        /// <param name="args">The <see cref="PathEventArgs"/> instance containing the event data.</param>
        protected virtual void OnDirectoryVisited(PathEventArgs args)
        {
            if (this.DirectoryVisited != null)
            {
                this.DirectoryVisited(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="FileVisited" /> event.
        /// </summary>
        /// <param name="args">The <see cref="PathEventArgs"/> instance containing the event data.</param>
        protected virtual void OnFileVisited(PathEventArgs args)
        {
            if (this.FileVisited != null)
            {
                this.FileVisited(this, args);
            }
        }

        /// <summary>
        /// Parses the existing WiX document.
        /// </summary>
        private void ParseExistingWix()
        {
            this.componentGuids = new Dictionary<string, Guid>();
            this.componentIds = new Dictionary<Guid, string>();
            this.directoryPaths = new Dictionary<string, string>();
            this.directoryPathToIds = new Dictionary<string, string>();
            this.filePaths = new Dictionary<string, string>();
            this.filePathToIds = new Dictionary<string, string>();

            var namespaceManager = new XmlNamespaceManager(this.Wix.NameTable);
            namespaceManager.AddNamespace("wix", WixNamespaces.Wix);

            XmlNodeList componentNodes = this.Wix.SelectNodes(
                "/wix:Wix/wix:Fragment/wix:ComponentGroup/wix:Component",
                namespaceManager);

            if (componentNodes != null)
            {
                Logger.Info("{0} components found.", componentNodes.Count);

                foreach (XmlNode componentNode in componentNodes)
                {
                    Debug.Assert(componentNode.Attributes != null, "componentNode.Attributes != null");
                    Guid guid = Guid.Parse(componentNode.Attributes["Guid"].Value);
                    string id = componentNode.Attributes["Id"].Value;
                    this.componentGuids[id] = guid;
                    this.componentIds[guid] = id;
                }
            }

            XmlNodeList directoryNodes = this.Wix.SelectNodes(
                "/wix:Wix/wix:Fragment//wix:Directory",
                namespaceManager);

            if (directoryNodes != null)
            {
                Logger.Info("{0} directories found.", directoryNodes.Count);

                foreach (XmlNode directoryNode in directoryNodes)
                {
                    Debug.Assert(directoryNode.Attributes != null, "directoryNode.Attributes != null");
                    string id = directoryNode.Attributes["Id"].Value;
                    string path = this.GetDirectoryPath(directoryNode);
                    this.directoryPaths[id] = path;
                    this.directoryPathToIds[path] = id;
                }
            }

            XmlNodeList fileNodes = this.Wix.SelectNodes(
                "/wix:Wix/wix:Fragment/wix:ComponentGroup/wix:Component/wix:File",
                namespaceManager);

            if (fileNodes != null)
            {
                Logger.Info("{0} files found.", fileNodes.Count);

                foreach (XmlNode fileNode in fileNodes)
                {
                    Debug.Assert(fileNode.Attributes != null, "fileNode.Attributes != null");
                    string id = fileNode.Attributes["Id"].Value;
                    string path = this.GetFilePath(fileNode);
                    this.filePaths[id] = path;
                    this.filePathToIds[path] = id;
                }
            }
        }

        /// <summary>
        /// Generates a new WiX document.
        /// </summary>
        private void GenerateNewWix()
        {
            if (this.componentGuids == null)
            {
                this.componentGuids = new Dictionary<string, Guid>();
            }

            if (this.componentIds == null)
            {
                this.componentIds = new Dictionary<Guid, string>();
            }

            if (this.directoryPaths == null)
            {
                this.directoryPaths = new Dictionary<string, string>();
            }

            if (this.directoryPathToIds == null)
            {
                this.directoryPathToIds = new Dictionary<string, string>();
            }

            if (this.filePaths == null)
            {
                this.filePaths = new Dictionary<string, string>();
            }

            if (this.filePathToIds == null)
            {
                this.filePathToIds = new Dictionary<string, string>();
            }

            this.Writer.WriteStartElement("Wix", WixNamespaces.Wix);
            this.Writer.WriteStartElement("Fragment");

            this.WriteWixProperty(this.parameters.FragmentPropertyName, "1");
            this.WriteWixDefine("SourceRoot", this.parameters.SourceRoot);

            this.Writer.WriteStartElement("DirectoryRef");
            this.Writer.WriteAttributeString("Id", this.parameters.ContainerDirectoryId);
            this.DiscoverDirectories();
            this.Writer.WriteEndElement();   // DirectoryRef

            if (!string.IsNullOrEmpty(this.parameters.ComponentGroupId))
            {
                this.Writer.WriteStartElement("ComponentGroup");
                this.Writer.WriteAttributeString("Id", this.parameters.ComponentGroupId);
            }

            this.DiscoverFiles();

            if (!string.IsNullOrEmpty(this.parameters.ComponentGroupId))
            {
                this.Writer.WriteEndElement();  // ComponentGroup
            }

            this.Writer.WriteEndElement();   // Fragment
            this.Writer.WriteEndElement();   // Wix
        }

        /// <summary>
        /// Discovers all directories.
        /// </summary>
        private void DiscoverDirectories()
        {
            Logger.Info("Discovering directories");

            foreach (string directory in Directory.GetDirectories(this.rootPath))
            {
                this.DiscoverDirectories(directory);
            }
        }

        /// <summary>
        /// Discovers all directories from the specified directory.
        /// </summary>
        /// <param name="path">The path of the directory which subdirectories will be discovered.</param>
        private void DiscoverDirectories(string path)
        {
            this.OnDirectoryVisited(new PathEventArgs(path));

            string id;
            if (!this.directoryPathToIds.TryGetValue(path, out id))
            {
                id = "dir_" + (this.parameters.IdPrefix ?? string.Empty) + this.GenerateIdBasedOnPath(
                    path,
                    "dir_",
                    this.parameters.IdPrefix,
                    this.directoryPaths)
                    .Replace("-", "_");

                this.directoryPaths.Add(id, path);
                this.directoryPathToIds.Add(path, id);
            }

            this.BeginDirectoryElement(id, Path.GetFileName(path));
            
            foreach (string directory in Directory.GetDirectories(path))
            {
                this.DiscoverDirectories(directory);
            }

            this.EndDirectoryElement();
        }

        /// <summary>
        /// Discovers all files.
        /// </summary>
        private void DiscoverFiles()
        {
            Logger.Info("Discovering files");
            this.DiscoverFiles(this.rootPath);
        }

        /// <summary>
        /// Discovers all files from the specified directory.
        /// </summary>
        /// <param name="path">The path of the directory which files will be discovered.</param>
        private void DiscoverFiles(string path)
        {
            this.OnDirectoryVisited(new PathEventArgs(path));

            string[] files = Directory.GetFiles(path);
            if (files.Any())
            {
                string directoryId;
                if (path == this.rootPath)
                {
                    directoryId = this.parameters.ContainerDirectoryId;
                }
                else
                {
                    directoryId = this.directoryPathToIds[path].Replace("-", "_");
                }

                foreach (string filePath in files)
                {
                    this.OnFileVisited(new PathEventArgs(filePath));

                    string id;
                    if (!this.filePathToIds.TryGetValue(filePath, out id))
                    {
                        id = "file_" + (this.parameters.IdPrefix ?? string.Empty) + this.GenerateIdBasedOnPath(
                            filePath,
                            "file_",
                            this.parameters.IdPrefix,
                            this.filePaths)
                            .Replace("-", "_");

                        this.filePaths.Add(id, filePath);
                        this.filePathToIds.Add(filePath, id);
                    }

                    Guid componentGuid;
                    string componentId = "cmp_" + id.Replace("file_", string.Empty);
                    if (!this.componentGuids.TryGetValue(componentId, out componentGuid))
                    {
                        componentGuid = Guid.NewGuid();
                    }
                    
                    this.BeginComponentElement(componentGuid, componentId, directoryId);
                    string fileSource = "$(var.SourceRoot)" + filePath.Replace(this.rootPath, string.Empty);
                    this.WriteFileElement(id, fileSource);
                    this.EndComponentElement();
                }
            }

            foreach (string directory in Directory.GetDirectories(path))
            {
                this.DiscoverFiles(directory);
            }
        }

        /// <summary>
        /// Gets the physical path of the specified WiX <c>Directory</c> node.
        /// </summary>
        /// <param name="directoryNode">The WiX directory node.</param>
        /// <returns>The full path to the directory represented by the specified node.</returns>
        private string GetDirectoryPath(XmlNode directoryNode)
        {
            var path = new List<string>();

            while ((directoryNode != null) && (directoryNode.Name == "Directory"))
            {
                /* ReSharper disable PossibleNullReferenceException */
                path.Add(directoryNode.Attributes["Name"].Value);
                /* ReSharper restore PossibleNullReferenceException*/
                directoryNode = directoryNode.ParentNode;
            }

            return ((IEnumerable<string>)path).Reverse().Aggregate(this.rootPath, Path.Combine);
        }

        /// <summary>
        /// Gets the physical path of the specified WiX <c>File</c> node.
        /// </summary>
        /// <param name="fileNode">The WiX file node.</param>
        /// <returns>The full path to the file represented by the specified node.</returns>
        private string GetFilePath(XmlNode fileNode)
        {
            /* ReSharper disable PossibleNullReferenceException */
            string source = fileNode.Attributes["Source"].Value;
            /* ReSharper restore PossibleNullReferenceException */

            return Path.Combine(
                this.parameters.SourceRoot,
                source.Replace("$(var.SourceRoot)", this.rootPath));
        }

        /// <summary>
        /// Generates a unique Wix identifier based on the specified path.
        /// </summary>
        /// <param name="path">The full path.</param>
        /// <param name="prefix">The prefix of the identifiers in the dictionary.</param>
        /// <param name="prefix2">The second identifier prefix.</param>
        /// <param name="ids">Dictionary of existing identifiers.</param>
        /// <returns>The generated unique identifier.</returns>
        private string GenerateIdBasedOnPath(
            string path,
            string prefix,
            string prefix2,
            IDictionary<string, string> ids)
        {
            if (prefix2 == null)
            {
                prefix2 = string.Empty;
            }

            string ext = string.Empty;
            if (File.Exists(path))
            {
                ext = (Path.GetExtension(path) ?? string.Empty).ToUpperInvariant();
                path = Path.GetDirectoryName(path) + "\\" + Path.GetFileNameWithoutExtension(path);
            }

            string id = path
                .Replace(this.rootPath + "\\", string.Empty)
                .Replace(this.rootPath, string.Empty)
                .Replace(".", string.Empty)
                .Replace("\\", "_")
                + ext.Replace(".", string.Empty);

            // Remove illegal characters from Wix identifier
            id = new string(id.Where(c => char.IsLetter(c) || char.IsDigit(c) || c == '_' || c  == '.').ToArray());

            while (id.Length > 66 - prefix2.Length)
            {
                int index = id.IndexOf('_');
                if (index != -1)
                {
                    id = id.Substring(index + 1);
                }
                else
                {
                    id = id.Substring(id.Length - 66);                    
                }
            }

            if (ids.ContainsKey(prefix + prefix2 + id))
            {
                for (int i = 0;; i++)
                {
                    string tmpId = id + i;
                    if (!ids.ContainsKey(prefix + prefix2 + tmpId))
                    {
                        id = tmpId;
                        break;
                    }
                }
            }

            return id;
        }
    }
}
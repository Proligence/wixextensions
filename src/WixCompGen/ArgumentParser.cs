﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentParser.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Parses the application's command-line arguments.
    /// </summary>
    internal sealed class ArgumentParser
    {
        /// <summary>
        /// Stores parsed command-line parameters.
        /// </summary>
        private readonly Dictionary<string, string> parameters;

        /// <summary>
        /// Stores parsed command-line switches.
        /// </summary>
        private readonly Dictionary<string, bool> switches;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParser"/> class.
        /// </summary>
        public ArgumentParser()
        {
            this.parameters = new Dictionary<string, string>();
            this.switches = new Dictionary<string, bool>();
        }

        /// <summary>
        /// Gets the name of the property generated to reference the Wix fragment.
        /// </summary>
        public string FragmentPropertyName
        {
            get { return this.GetParameter("FragmentPropertyName"); }
        }

        /// <summary>
        /// Gets the identifier of the container directory.
        /// </summary>
        public string ContainerDirectoryId
        {
            get { return this.GetParameter("ContainerDirectoryId"); }
        }

        /// <summary>
        /// Gets the root directory for source files.
        /// </summary>
        public string SourceRoot
        {
            get { return this.GetParameter("SourceRoot"); }
        }

        /// <summary>
        /// Gets the identifier of the component group which will contain the generated components.
        /// </summary>
        public string ComponentGroupId
        {
            get { return this.GetParameter("ComponentGroupId"); }
        }

        /// <summary>
        /// Gets the full path to the root source directory.
        /// </summary>
        public string SourcePath
        {
            get { return this.GetParameter("SourcePath"); }
        }

        /// <summary>
        /// Gets the name of the output file.
        /// </summary>
        public string Output
        {
            get { return this.GetParameter("Output"); }
        }

        /// <summary>
        /// Gets a value indicating whether the application is in WiX update mode.
        /// </summary>
        public bool UpdateMode
        {
            get { return this.GetSwich("Update"); }
        }

        /// <summary>
        /// Parses the specified command-line arguments.
        /// </summary>
        /// <param name="args">The arguments to parse..</param>
        public void Parse(string[] args)
        {
            foreach (string arg in args)
            {
                string str = arg;
                
                if (str.StartsWith("/"))
                {
                    str = str.Substring(1);
                }

                if (str.StartsWith("-"))
                {
                    str = str.Substring(1);
                }

                int index = str.IndexOf(':');
                if (index != -1)
                {
                    string name = str.Substring(0, index);
                    string value = str.Substring(index + 1);
                    this.parameters[name] = value;
                }
                else
                {
                    this.switches[str] = true;
                }
            }
        }

        /// <summary>
        /// Validates the parsed command-line arguments.
        /// </summary>
        public void Validate()
        {
            if (!this.parameters.ContainsKey("FragmentPropertyName"))
            {
                throw new ApplicationException("Required argument 'FragmentPropertyName' was not specified.");
            }

            if (!this.parameters.ContainsKey("ContainerDirectoryId"))
            {
                throw new ApplicationException("Required argument 'ContainerDirectoryId' was not specified.");
            }

            if (!this.parameters.ContainsKey("SourceRoot"))
            {
                throw new ApplicationException("Required argument 'SourceRoot' was not specified.");
            }

            if (!this.parameters.ContainsKey("ComponentGroupId"))
            {
                throw new ApplicationException("Required argument 'ComponentGroupId' was not specified.");
            }

            if (!this.parameters.ContainsKey("SourcePath"))
            {
                throw new ApplicationException("Required argument 'SourcePath' was not specified.");
            }

            if (!this.parameters.ContainsKey("Output"))
            {
                throw new ApplicationException("Required argument 'Output' was not specified.");
            }
        }

        /// <summary>
        /// Gets the value of the parameter with the specified name.
        /// </summary>
        /// <param name="name">The name of the parameter to get.</param>
        /// <returns>The value of the parameter or <c>null</c>.</returns>
        private string GetParameter(string name)
        {
            if (this.parameters.ContainsKey(name))
            {
                return this.parameters[name];
            }

            return null;
        }

        /// <summary>
        /// Gets the value of the switch with the specified name.
        /// </summary>
        /// <param name="name">The name of the switch to get.</param>
        /// <returns><c>true</c> if the switch is set; otherwise, <c>false</c>.</returns>
        private bool GetSwich(string name)
        {
            if (this.switches.ContainsKey(name))
            {
                return this.switches[name];
            }

            return false;
        }
    }
}
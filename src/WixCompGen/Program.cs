﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.WixCompGen
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Implements the application's entry point.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Implements the application's entry point.
        /// </summary>
        /// <param name="args">The application's command line arguments.</param>
        /// <returns>The application's exit code.</returns>
        public static int Main(string[] args)
        {
            PrintBanner();

            if (args.Length == 0)
            {
                PrintHelp();
                return 0;
            }

            return new Application().Run(args);
        }

        /// <summary>
        /// Prints the application's name and version.
        /// </summary>
        private static void PrintBanner()
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;

            Console.WriteLine("Proligence (R) WiX Component Generator, version " + version);
            Console.WriteLine("Copyright (C) Proligence. All rights reserved.");
            Console.WriteLine();
        }

        /// <summary>
        /// Prints the application's help screen.
        /// </summary>
        private static void PrintHelp()
        {
            Console.WriteLine(
@"Syntax:              WixCompGen.exe [ /FragmentPropertyName:ID
                       /ContainerDirectoryId:ID /SourceRoot:string
                       /ComponentGroupId:ID ] /SourcePath:path
                       /Output:path [ options ]

Description:           Generates a list of WiX components which contains the 
                       files from the specified directory.

Switches:

  /FragmentPropertyName:<ID>    The ID of the WiX property generated to 
                                reference the generated WiX fragment from
                                other WiX source files.
                                Example:
                                  /FragmentPropertyName:MyComponents

  /ContainerDirectoryId:<ID>    The ID of the directory in which the generated
                                files and directories will be contained.
                                Example:
                                  /ContainerDirectoryId:dir_MyRoot

  /SourceRoot:<string>          The WiX expression which will be included in
                                the 'Source' attributes of all generated fule
                                elements. This should be used to locate the
                                relative source files directory from the path
                                of the WiX project.
                                Example:
                                  /SourceRoot:""$(var.SolutionDir)..\build\Stage""

  /ComponentGroupId:<ID>        The identifier of the generated WiX component
                                group which will contain all generated
                                components.
                                components.
                                Example:
                                  /ComponentGroupId:cmp_MyComponents

  /SourcePath:<path>            The path to the directory which conains the
                                files and directories for which WiX components
                                will be generated.
                                Example:
                                  /SourcePath:""D:\MyProject\bin""

  /Output:<path>                The path to the wxs file to which generated
                                components will be written. If the file exists,
                                then its contents will be overwritten!
                                Example:
                                  /Output:""D:\MyProject\src\Setup\Components.wxs""

  /Update                       Update existing WiX source file.
");
        }
    }
}
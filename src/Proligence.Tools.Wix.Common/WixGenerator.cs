﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WixGenerator.cs" company="Proligence">
//   Copyright (C) 2012  Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Tools.Wix.Common
{
    using System;
    using System.Xml;

    /// <summary>
    /// Implements the base class for classes which generate Wix documents.
    /// </summary>
    public abstract class WixGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WixGenerator"/> class.
        /// </summary>
        /// <param name="writer">The XML writer to which generated Wix will be written.</param>
        protected WixGenerator(XmlWriter writer)
        {
            this.Writer = writer;
        }

        /// <summary>
        /// Gets or sets the XML writer to which generated Wix will be written.
        /// </summary>
        protected XmlWriter Writer { get; set; }

        /// <summary>
        /// Generates the Wix XML.
        /// </summary>
        public virtual void GenerateWix()
        {
            this.Writer.WriteStartElement("Wix", WixNamespaces.Wix);
            this.Writer.WriteStartElement("Fragment");
            
            this.GenerateWixInternal();

            this.Writer.WriteEndElement();   // Fragment
            this.Writer.WriteEndElement();   // Wix
        }

        /// <summary>
        /// Generates the Wix XML.
        /// </summary>
        protected virtual void GenerateWixInternal()
        {
        }

        /// <summary>
        /// Renders a Wix variable definition.
        /// </summary>
        /// <param name="name">The name of the variable.</param>
        /// <param name="value">The value of the variable.</param>
        protected virtual void WriteWixDefine(string name, string value)
        {
            this.Writer.WriteProcessingInstruction("define", name + " = " + value);
        }

        /// <summary>
        /// Writes a Wix property to the output XML.
        /// </summary>
        /// <param name="id">The property ID.</param>
        /// <param name="value">The property value.</param>
        protected virtual void WriteWixProperty(string id, string value)
        {
            this.Writer.WriteStartElement("Property");
            this.Writer.WriteAttributeString("Id", id);
            this.Writer.WriteAttributeString("Value", value);
            this.Writer.WriteEndElement();
        }

        /// <summary>
        /// Begins a directory element.
        /// </summary>
        /// <param name="id">The identifier of the directory.</param>
        /// <param name="name">The name of the directory.</param>
        protected virtual void BeginDirectoryElement(string id, string name)
        {
            this.Writer.WriteStartElement("Directory");
            this.Writer.WriteAttributeString("Id", id);
            this.Writer.WriteAttributeString("Name", name);
        }

        /// <summary>
        /// Ends a directory element.
        /// </summary>
        protected virtual void EndDirectoryElement()
        {
            this.Writer.WriteEndElement();
        }

        /// <summary>
        /// Begins a component element.
        /// </summary>
        /// <param name="guid">The GUID of the component.</param>
        /// <param name="id">The identifier of the component.</param>
        /// <param name="directory">Containing directory identifier.</param>
        protected virtual void BeginComponentElement(Guid guid, string id = null, string directory = null)
        {
            if (id == null)
            {
                id = "cmp_" + guid.ToString().Replace("-", "_");
            }

            this.Writer.WriteStartElement("Component");
            this.Writer.WriteAttributeString("Id", id);
            this.Writer.WriteAttributeString("Guid", guid.ToString().ToUpper());
            
            if (!string.IsNullOrEmpty(directory))
            {
                this.Writer.WriteAttributeString("Directory", directory);
            }
        }

        /// <summary>
        /// Ends a component element.
        /// </summary>
        protected virtual void EndComponentElement()
        {
            this.Writer.WriteEndElement();
        }

        /// <summary>
        /// Writes a file element.
        /// </summary>
        /// <param name="id">The file's identifier.</param>
        /// <param name="source">The source file name.</param>
        /// <param name="keyPath"><c>true</c> to set file as key path; otherwise, <c>false</c>.</param>
        /// <param name="vital"><c>true</c> to mark file as vital; otherwise, <c>false</c>.</param>
        protected virtual void WriteFileElement(string id, string source, bool keyPath = true, bool vital = true)
        {
            this.Writer.WriteStartElement("File");
            this.Writer.WriteAttributeString("Id", id);
            this.Writer.WriteAttributeString("Source", source);
            this.Writer.WriteAttributeString("KeyPath", keyPath ? "yes" : "no");
            this.Writer.WriteAttributeString("Vital", vital ? "yes" : "no");
            this.Writer.WriteEndElement();
        }
    }
}